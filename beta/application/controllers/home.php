<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index() {
		
		$this->template->set('message', 'content area goes here');
		$this->template->current_view = ('template/home_view');
		$this->template->render();
	
	}
}