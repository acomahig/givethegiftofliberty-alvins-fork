<!DOCTYPE HTML>
<html>
<head>
	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $site_name ?></title> 
	<link href='http://fonts.googleapis.com/css?family=Crete+Round:400,400italic' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' href="<?php echo base_url();?>/style/main.css" type="text/css" media="screen" />
	<link rel='stylesheet' href="<?php echo base_url();?>/style/story.css" type="text/css" media="screen" />
	<script type='text/javascript' src='<?php echo base_url();?>/script/jquery-1.8.2.min.js'></script>
	<script type='text/javascript' src='<?php echo base_url();?>/script/main.js'></script>
</head>

<body>
<header>
  <div id="headerdark">
    	<div id="headerdarkContent"></div>
 </div>
  <div id="headerLight">
        <span id="ribbonleft">&nbsp;</span>
        <a id="logo" href="" title="Give the Gift of Liberty">&nbsp;</a>
        <span id="ribbon">&nbsp;</span>
        <div id="upperRight">
        	<a href="">Student Login</a> | <a href="">Create Account</a>
             <form style="margin-top: 7px;" ><a href="" id="facebook" class="socialButton">&nbsp;</a>
            <a href="" id="twitter" class="socialButton">&nbsp;</a>
            <a href="" id="googleplus" class="socialButton">&nbsp;</a>
         <input value="Search" id="websiteSearchinput" type="text" class="searchField" /><input id="searchButton" name="searchGo" type="submit" value="Go!"></form>
            
        </div>
        <div id="linkContainer">
        	<ul>
           	  <li><a href="" class="withoutborder">Home</a></li>
              <li><a href="" class="withborder" >How it Works</a></li>
              <li><a href="" class="withborder">Why Give</a></li>
              <li><a href="" class="withborder">Student Needs</a></li>
              <li><a href="" class="withborder">Success Stories</a></li>
              <li><a href="" class="withborder">About SFL</a></li>
            </ul>
        </div>
  </div>
</header>
<?php echo $this->template->message(); ?>    
<?php echo $this->template->yield(); ?>
<footer>
	<div id="footerContent">
    <span id="footerRibbon">&nbsp;</span>
    
	<div id="footerLink">
    	<ul>
          <li><a href="">Home</a></li>
          <li><a href="" class="footerLinkLine">How it Works</a></li>
          <li><a href="" class="footerLinkLine">Why Give</a></li>
          <li><a href="" class="footerLinkLine">Student Needs</a></li>
          <li><a href="" class="footerLinkLine">Success Stories</a></li>
          <li><a href="" class="footerLinkLine">About SFL</a></li>
          <li><a href="" class="footerLinkLine">Student Login</a></li>
          <li><a href="" class="footerLinkLine">Site Map</a></li>
        </ul>
    </div>
    <div id="footerColumn1">
    	<a href="" id="footerLogo">&nbsp;</a>
        <p>
        <a href="">Privacy Policy</a>
    	| <a href="">Terms of Service</a>
        </p>
    </div>
    <div id="footerColumn2">
   	  <img src="<?php echo base_url();?>/images/Gift_of_Giving.gif" width="119" height="119" alt="Give the Gift of Liberty"></div>
    <div id="footerColumn3">
    	<p>Copyright 2012.  Students For Liberty
    	<br>All Rights Reserved</p>
        <p>Web design by <a href="">Market Aces</a><p>
    </div>
    </div>
    <div id="footerDark">
    </div>
    
</footer>
</body>
</html>
