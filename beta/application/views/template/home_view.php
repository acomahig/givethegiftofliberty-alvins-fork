<link rel='stylesheet' href="<?php echo base_url();?>/style/homepage.css" type="text/css" media="screen" />
<div id="contentArea">
   <div id="content">
    <section class="contentTop-wrap">
      <div class="leftHeader">
        <div class="socialMedia">
          <ul>
            <li class="fb"><a href="#"></a></li>
            <li class="twiter"><a href="#"></a></li>
            <li class="plus"><a href="#"></a></li>
          </ul>
        </div>
        <section class="slider">
          <p>Accompanying Photograph</p>
          <ul>
            <li class="active"><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li class="pause"><a href="#"></a></li>
          </ul>
        </section>
      </div>  
      <section class="featured">
        <div class="header"> 
          <h1>Featured Student Need</h1> 
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet mollis porttitor. Cras ictum lobortis mauris, ac hendrerit nulla blandit in. Praesent nisl nunc, pulvinar at ultrices ac, ornare at elit. Nulla vulputate, lectus cursus posuere condimentum, justo mi fringilla justo, vel semper elit.</p>
        </div>
        <div class="clear"></div>  
        <div class="donate">
        	
         <div class="meter">      	
			<div class="meter-inner">
				<span style="width: 25%;"><strong>$150</strong></span>
			</div>
		 </div>
		 <div class="torch"><span class="torch-bg"></span></div>   
	      <div class="btn-donate">
	      	<p><a href="#">Donate</a></p>
	      </div>
	
	      <div class="range">
		  	<span class="left">$0</span>
          	<span class="right">$200</span>	
		  </div>
          <div class="clear"></div>
          <div class="learnMore">
            <p><a href="#">Learn More</a></p>
          </div>
          <div class="clear"></div>
        </div>
      </section>
      <div class="clear"></div>
    </section>
    <section class="mainContent">
        <aside id="pageAside">
          <div class="advertise">
            <h1>Ad Goes Here 200 x ...</h1>
          </div>
          <div class="searchBtn">
            <p><a href="#">Search Needs</a></p>
          </div>
          <div class="date">
            <form>
              <h1>Sort By:</h1>
              <input type="radio" name="date" value="deadline">Deadline Date<br> 
              <input type="radio" name="date" value="created" checked>Date Created
            </form>
          </div>
          <div class="school">
            <p>School</p>
            <input class="inputSchool" type="text" name="SchoolName" placeholder="Type  in school name here">
            <p>Need Type</p>
              <form>
              	<ul>
                <li><input type="radio" name="needType" value="deadline">Travel Scholarships</li>
                <li><input type="radio" name="needType" value="deadline">Protests</li>
                <li><input type="radio" name="needType" value="deadline">Campus Events</li>
                <li><input type="radio" name="needType" value="deadline">Books</li>
                <li><input type="radio" name="needType" value="deadline">Other Group Supplies</li>
                </ul>
              </form>
            <p>State/Country</p>
            <select class="selectState">
              <option selected="selected" value="volvo">MD</option>
              <option value="saab">Saab</option>
              <option value="vw">VW</option>
            </select>
            <select class="selectState">
              <option selected="selected" value="volvo">United States of America</option>
              <option value="saab">Philippines</option>
              <option value="vw">Brazil</option>
            </select>
            <p>Search Tags</p>
            <input class="searchTag" type="text" name="searchTag">             
          </div>
          <div class="clear"></div>
          <div class="borderBottom"></div>
          <div class="advertise advertise--2">
            <h1>Ad Goes Here 200 x ...</h1>
          </div>
          <div class="advertise advertise--2">
            <h1>Ad Goes Here 200 x ...</h1>
          </div>
          <div class="advertise advertise--2">
            <h1>Ad Goes Here 200 x ...</h1>
          </div>                    
        </aside>
        <section id="pageSection">
          <article class="leftCol">
            <section class="headerTitle">
              <h1>Most Recent Student Needs</h1>
            </section>
            <section class="bodyContent">
              <h2>Project Name Goes Here</h2>
              <img class="featuredImage" src="images/img-Content.jpg" alt="content image">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta.</p>
         
              <div class="meter-small">
              	<div class="meter-inner-small">
					<span><strong>$325</strong></span>				
              	</div>      			
			 </div>
		 <div class="torch-small"><span class="torch-bg"></span></div>   
              <div class="donateBtn">
                <p><a href="#">Donate</a></p>
              </div>
              <div class="clear"></div>
              <p class="donateRate">$0</p>
              <p class="donateRate right">$700</p>
              <div class="clear"></div>
              <a href="#" class="moreBtn">Learn More</a>
              <span>10 days to go!</span>
              <div class="clear"></div>
            </section>
            <section class="bodyContent">
              <h2>Project Name Goes Here</h2>
              <img class="featuredImage" src="images/img-Content.jpg" alt="content image">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta.</p>
             <div class="meter-small">
              	<div class="meter-inner-small">
					<span><strong>$325</strong></span>				
              	</div>      			
			 </div>
		 <div class="torch-small"><span class="torch-bg"></span></div>
              <div class="donateBtn">
                <p><a href="#">Donate</a></p>
              </div>
              <div class="clear"></div>
              <p class="donateRate">$0</p>
              <p class="donateRate right">$1,000</p>
              <div class="clear"></div>
              <a href="#" class="moreBtn">Learn More</a>
              <span>10 days to go!</span>
              <div class="clear"></div>
            </section>
            <section class="bodyContent">
              <h2>Project Name Goes Here</h2>
              <img class="featuredImage" src="images/img-Content.jpg" alt="content image">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta.</p>
              <div class="meter-small">
              	<div class="meter-inner-small">
					<span><strong>$325</strong></span>				
              	</div>      			
			 </div>
		 <div class="torch-small"><span class="torch-bg"></span></div>   
              <div class="donateBtn">
                <p><a href="#">Donate</a></p>
              </div>
              <div class="clear"></div>
              <p class="donateRate">$0</p>
              <p class="donateRate right">$700</p>
              <div class="clear"></div>
              <a href="#" class="moreBtn">Learn More</a>
              <span>10 days to go!</span>
              
            </section>
            <div class="clear"></div>
            <div class="footerbg"></div>                          
          </article>
          <article class="rightCol">
            <section class="headerTitle">
              <h1>Upcoming Deadlines</h1>
            </section>
            <section class="bodyContent">
              <h2>Project Name Goes Here</h2>
              <img class="featuredImage" src="images/img-Content.jpg" alt="content image">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta.</p>
              <div class="meter-small">
              	<div class="meter-inner-small">
					<span><strong>$750</strong></span>				
              	</div>      			
			 </div>
		 	<div class="torch-small"><span class="torch-bg"></span></div>   
              <div class="donateBtn">
                <p><a href="#">Donate</a></p>
              </div>
              <div class="clear"></div>
              <p class="donateRate">$0</p>
              <p class="donateRate right">$700</p>
              <div class="clear"></div>
              <a href="#" class="moreBtn">Learn More</a>
              <span>15 hrs to go!</span>
              <div class="clear"></div>
            </section>
            <section class="bodyContent">
              <h2>Project Name Goes Here</h2>
              <img class="featuredImage" src="images/img-Content.jpg" alt="content image">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta.</p>
              <div class="meter-small">
              	<div class="meter-inner-small">
					<span><strong>$1,150</strong></span>				
              	</div>      			
			 </div>
		 	<div class="torch-small"><span class="torch-bg"></span></div>   
              <div class="donateBtn">
                <p><a href="#">Donate</a></p>
              </div>
              <div class="clear"></div>
              <p class="donateRate">$0</p>
              <p class="donateRate right">$1,000</p>
              <div class="clear"></div>
              <a href="#" class="moreBtn">Learn More</a>
              <span>27 hours to go!</span>
              <div class="clear"></div>
            </section>
            <section class="bodyContent">
              <h2>Project Name Goes Here</h2>
              <img class="featuredImage" src="images/img-Content.jpg" alt="content image">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta.</p>
              <div class="meter-small">
              	<div class="meter-inner-small">
					<span><strong>$325</strong></span>				
              	</div>      			
			 </div>
		 	<div class="torch-small"><span class="torch-bg"></span></div>   
              <div class="donateBtn">
                <p><a href="#">Donate</a></p>
              </div>
              <div class="clear"></div>
              <p class="donateRate">$0</p>
              <p class="donateRate right">$700</p>
              <div class="clear"></div>
              <a href="#" class="moreBtn">Learn More</a>
              <span>30 hours to go!</span>
              
            </section> 
            <div class="clear"></div>
            <div class="footerbg"></div>
          </article>
          <div class="clear"></div>
          <section class="sectionFooter">
            <h1>Success Stories – Giving Liberty Changes Lives</h1>
              <ul>
                <li>
                  <div class="footerBody">
                    <img src="images/img-Content2.jpg" alt="footer image">
                    <h2>Group Name Goes Here</h2>
                    <h2>Project Name Goes Here</h2>
                    <h2>Amount Raised: <span>$2,500</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta condimentum...</p>
                    <a href="#" class="moreBtn">View More</a>
                  </div>
                </li>
                <li>
                  <div class="footerBody">
                    <img src="images/img-Content2.jpg" alt="footer image">
                    <h2>Group Name Goes Here</h2>
                    <h2>Project Name Goes Here</h2>
                    <h2>Amount Raised: <span>$2,500</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sollicitudin blandit lacus, eu hendrerit velit rhoncus tempor. Curabitur ut ante ac augue pretium dictum. Etiam tristique, mi dictum porta condimentum...</p>
                    <a href="#" class="moreBtn">View More</a>
                    <div class="clear"></div>
                    <a href="#" class="viewAll">View all success stories</a>
                  </div>
                </li>                
              </ul>
          </section>          
        </section>
        <div class="clear"></div>
    </section>
</div>
</div>